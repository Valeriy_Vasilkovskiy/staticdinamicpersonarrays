package main;

import java.util.Arrays;

public class StaticArray implements InterfaceArrays{

    int[] arr = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


    @Override
    public void addElement (int index, int element) {
        if (index < 0 || index > arr.length || arr.length == 0) {
            System.out.println("Error");
        } else {
            index -= 1;
            for (int i = 0; i < arr.length + 1; i++) {
                if (i == index) {
                    arr[i] = element;
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void addArray(int[] arr1) {
        if (arr.length < arr1.length || arr.length == 0) {
            System.out.println("Error");
        } else {
            for (int i = 0; i != arr1.length; i++) {
                arr[i] = arr1[i];
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void addStart(int element) {
        if (arr.length == 0) {
            System.out.println("Error");
        }
        arr[0] = element;
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void addEnd(int element) {
        if (arr.length == 0) {
            System.out.println("Error");
        }
        arr[arr.length - 1] = element;
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void deleteElement(int index) {
        if (arr.length == 0 || index > arr.length - 1 || index < 0) {
            System.out.println("Error");
        } else {
            for (int i = 0; i != arr.length; i++) {
                if (i == index) {
                    arr[i] = 0;
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void deleteAll() {
        for (int i = 0; i != arr.length; i++) {
            arr[i] = 0;
        }
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void deleteStart() {
        arr[0] = 0;
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void deleteEnd() {
        arr[arr.length - 1] = 0;
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void sortMaxMin() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
        } else {
            for (int i = 0; i != arr.length; i++) {
                for (int j = 0; j != arr.length - 1; j++) {
                    if (arr[j + 1] > arr[j]) {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void sortMinMax() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
        } else {
            for (int i = 0; i != arr.length; i++) {
                for (int j = 0; j != arr.length - 1; j++) {
                    if (arr[j + 1] < arr[j]) {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void reverse() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
        } else {
            for (int i = 0; i != arr.length / 2; i++) {
                int temp = arr[i];
                arr[i] = arr[arr.length - i - 1];
                arr[arr.length - i - 1] = temp;
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void halfReverse() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
        } else {
            for (int i = 0; i != (arr.length / 2); i++) {
                int temp = arr[i];
                arr[i] = arr[arr.length / 2 + i];
                arr[arr.length / 2 + i] = temp;
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    @Override
    public void StringArrayToString() {
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void print() {
        for (int i = 0; i != arr.length; i++) {
            System.out.print(arr[i] + "; ");
        }
        System.out.println();
    }

    @Override
    public int[] get() {
        return arr;
    }
}
