package main;

public interface InterfaceArrays {

    void addElement(int i, int e);

    void addArray(int[] arr);

    void addStart(int e);

    void addEnd(int e);

    void deleteElement(int e);

    void deleteAll();

    void deleteStart();

    void deleteEnd();

    void sortMaxMin();

    void sortMinMax();

    void reverse();

    void halfReverse();

    void StringArrayToString();

    void print();

    int[] get();
}
