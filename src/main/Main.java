package main;

import person.*;

public class Main {
    public static void main(String[] args) {

        int [] data = {1,2,3,4,2,6,7,8};

        InterfaceArrays faceDynamic = new DynamicArray();

//        faceDynamic.addElement(3, 88);
//        faceDynamic.addElement(4,99);
//        faceDynamic.addArray(data);
//        faceDynamic.addStart(5);
//        faceDynamic.addEnd(8);
//        faceDynamic.deleteElement(10);
//        faceDynamic.deleteStart();
//        faceDynamic.deleteEnd();
//        faceDynamic.sortMaxMin();
//        faceDynamic.sortMinMax();
//        faceDynamic.reverse();
//        faceDynamic.halfReverse();
//        faceDynamic.print();
//        faceDynamic.StringArrayToString();
//        faceDynamic.get();
//        faceDynamic.deleteAll();
        //===================================================
        InterfaceArrays faceStatic = new StaticArray();

//        faceStatic.addElement(3, 88);
//        faceStatic.addElement(4,99);
//        faceStatic.addArray(data);
//        faceStatic.addStart(5);
//        faceStatic.addEnd(8);
//        faceStatic.deleteElement(9);
//        faceStatic.deleteStart();
//        faceStatic.deleteEnd();
//        faceStatic.sortMaxMin();
//        faceStatic.sortMinMax();
//        faceStatic.reverse();
//        faceStatic.halfReverse();
//        faceStatic.print();
//        faceStatic.StringArrayToString();
//        faceStatic.get();
//        faceStatic.deleteAll();
        //=====================================================
        PersonInterface personFace = new PersonArray();

        Person[] temp = {
                new Person(1, "Alex", 19),
                new Person(3, "Jake", 18)};

        personFace.addElement(1, new Person(1,"Valera",24));
        personFace.addElement(1, new Person(1,"Valera",24));
        personFace.addArray(temp);
        personFace.deleteElement(3);
        personFace.print();
        personFace.addEnd(new Person(5,"Kay", 22));
        personFace.addStart(new Person(4,"Robert", 14));
        personFace.print();
        personFace.StringArrayToString();
        personFace.sortById();
        personFace.deleteEnd();
        personFace.deleteStart();
        personFace.deleteAll();
    }
}

