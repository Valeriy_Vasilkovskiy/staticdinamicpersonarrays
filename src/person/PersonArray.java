package person;

import org.omg.CORBA.PERSIST_STORE;

import java.util.Arrays;

public class PersonArray implements PersonInterface {
    Person[] arr = new Person[0];

    @Override
    public void addElement(int index, Person person) {
        if (person.getId() < 0 || index < 1) {
            System.out.println("Error");
        } else {
            for (Person element : arr) {
                if (element.getId() == person.getId()) {
                    System.out.println("Sorry, but this ID is already in use");
                    System.out.println("=======================================");
                    return;
                }
            }
            Person[] temp = new Person[arr.length + 1];
            for (int i = 0; i != arr.length; i++) {
                if (i == person.getId()) {
                    temp[i] = person;
                } else {
                    temp[i] = arr[i];
                }
            }
            temp[index - 1] = person;
            arr = temp;
            print();
        }
    }

    @Override
    public void addArray(Person[] person) {
        Person[] temp = new Person[arr.length + person.length];
        for (int i = 0; i != arr.length; i++) {
            temp[i] = arr[i];
        }
        for (int i = 0; i != person.length; i++) {
            temp[arr.length + i] = person[i];
        }
        arr = temp;
        print();
    }

    @Override
    public void addStart(Person person) {
        for (Person element : arr) {
            if (element.getId() == person.getId()) {
                System.out.println("Sorry, but this ID is already in use");
                System.out.println("=======================================");
                return;
            }
        }
        Person[] temp = new Person[arr.length + 1];
        temp[0] = person;
        for (int i = 0; i != arr.length; i++) {
            temp[i + 1] = arr[i];
        }
        arr = temp;
        print();
    }

    @Override
    public void addEnd(Person person) {
        for (Person element : arr) {
            if (element.getId() == person.getId()) {
                System.out.println("Sorry, but this ID is already in use");
                System.out.println("=======================================");
                return;
            }
        }
        Person[] temp = new Person[arr.length + 1];
        temp[temp.length - 1] = person;
        for (int i = 0; i != arr.length; i++) {
            temp[i] = arr[i];
        }
        arr = temp;
        print();
    }

    @Override
    public void deleteElement(int id) {
        Person[] temp = new Person[arr.length - 1];
        int k = 0;
        for (int i = 0; i != arr.length; i++) {
            if (arr[i].getId() == id) {
                k = i;
            }
        }
        for (int i = 0; i != k; i++) {
            temp[i] = arr[i];
        }
        for (int i = k + 1; i != arr.length; i++) {
            temp[i - 1] = arr[i];
        }
        arr = temp;
    }


    @Override
    public void deleteAll() {
        Person[] temp = new Person[0];
        System.out.println(Arrays.toString(temp));
        arr = temp;
    }

    @Override
    public void deleteStart() {
        if (arr.length == 0) {
            System.out.println("Error");
        } else {
            Person[] temp = new Person[arr.length - 1];
            for (int i = 1; i < arr.length; i++) {
                temp[i - 1] = arr[i];
            }
            arr = temp;
            print();
        }
    }

    @Override
    public void deleteEnd() {
        if (arr.length == 0) {
            System.out.println("Error");
        } else {
            Person[] temp = new Person[arr.length - 1];
            for (int i = 0; i < arr.length - 1; i++) {
                temp[i] = arr[i];
            }
            arr = temp;
            print();
        }
    }

    @Override
    public void StringArrayToString() {
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void print() {
        for (int i = 0; i != arr.length; i++) {
            System.out.println("Id = " + arr[i].getId() + ", Name = " + arr[i].getName() + ", Age = " + arr[i].getAge() + "; ");
        }
        System.out.println("=========================================");
    }

    @Override
    public Person[] get() {
        return arr;
    }

    @Override
    public void sortById() {
        for (int k = 0; k != arr.length-1; k++) {
            for (int z =1; z !=arr.length; z++) {
                if (arr[k].getId() == arr[z].getId()) {
                    deleteElement(z);
                }
            }
        }
        if ( arr.length == 1) {
            print();
        } else {
            for (int i = 0; i != arr.length; i++) {
                for (int j = 0; j != arr.length - 1; j++) {
                    if (arr[j + 1].getId() < (arr[j]).getId()) {
                        Person temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            print();
        }
    }
}