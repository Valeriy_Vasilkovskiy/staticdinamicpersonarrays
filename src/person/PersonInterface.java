package person;

public interface PersonInterface {
    void addElement(int id, Person person);

    void addArray(Person[] person);

    void addStart(Person person);

    void addEnd(Person person);

    void deleteElement(int id);

    void deleteAll();

    void deleteStart();

    void deleteEnd();

    void StringArrayToString();

    void print();

    Person[] get();

    void sortById();


}

